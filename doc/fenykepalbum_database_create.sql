-- clear database
DROP TABLE imageinfo;
DROP TABLE userplaces;
DROP TABLE ratings;
DROP TABLE users;
DROP TABLE images;
DROP TABLE cities;
DROP TABLE counties;
DROP TABLE countries;
DROP TABLE categories;

DROP SEQUENCE users_seq;
DROP SEQUENCE images_seq;
DROP SEQUENCE countries_seq;
DROP SEQUENCE counties_seq;
DROP SEQUENCE cities_seq;
DROP SEQUENCE categories_seq;
--create tables
CREATE TABLE users (
	id 				  INTEGER NOT NULL,
	username 		VARCHAR(20) UNIQUE NOT NULL,
	password 		VARCHAR(40) NOT NULL,
  CONSTRAINT users_pk PRIMARY KEY (id)
);
CREATE TABLE images (
	id 				  INTEGER NOT NULL,
	hyperlink 	VARCHAR(200) NOT NULL,
  CONSTRAINT images_pk PRIMARY KEY (id)
);
CREATE TABLE countries (
  id          INTEGER NOT NULL,
	name 			  VARCHAR(20) NOT NULL,
  CONSTRAINT countries_pk PRIMARY KEY (id)
);
CREATE TABLE counties (
  id          INTEGER NOT NULL,
	name 			  VARCHAR(20) NOT NULL,
  country_id   INTEGER NOT NULL,
  CONSTRAINT fk_counties
    FOREIGN KEY(country_id) REFERENCES countries(id) ON DELETE CASCADE,
  CONSTRAINT counties_pk PRIMARY KEY (id)
);
CREATE TABLE cities (
  id          INTEGER NOT NULL,
	name 			  VARCHAR(20) NOT NULL,
  county_id   INTEGER NOT NULL,
  CONSTRAINT fk_cities
    FOREIGN KEY(county_id) REFERENCES counties(id) ON DELETE CASCADE,
  CONSTRAINT cities_pk PRIMARY KEY (id)
);
CREATE TABLE categories (
	id          INTEGER NOT NULL,
	name 			  VARCHAR(20) NOT NULL,
  CONSTRAINT categories_pk PRIMARY KEY (id)
);
CREATE TABLE userplaces ( 
  user_id INTEGER PRIMARY KEY,
  city_id INTEGER NOT NULL,
  CONSTRAINT fk_userplaces
    FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY(city_id) REFERENCES cities(id) ON DELETE CASCADE
);
CREATE TABLE imageinfo (
  image_id INTEGER NOT NULL,
  user_id INTEGER NOT NULL,
  city_id INTEGER NOT NULL,
  category_id INTEGER NOT NULL,
  CONSTRAINT imageinfo_pk
    PRIMARY KEY(image_id),
  CONSTRAINT fk_imageinfo
    FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY(city_id) REFERENCES cities(id) ON DELETE CASCADE,
    FOREIGN KEY(image_id) REFERENCES images(id) ON DELETE CASCADE,
    FOREIGN KEY(category_id) REFERENCES categories(id) ON DELETE CASCADE
);
CREATE TABLE ratings (
	image_id      INTEGER NOT NULL,
  user_id       INTEGER NOT NULL,
  rate			    INTEGER NOT NULL,
  CONSTRAINT fk_ratings
    FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY(image_id) REFERENCES images(id) ON DELETE CASCADE,
  CONSTRAINT ratings_pk
    PRIMARY KEY(image_id, user_id)
	);
/

-- create autoincrements
CREATE SEQUENCE users_seq
START WITH 1 
INCREMENT BY 1 
NOMAXVALUE; 

CREATE OR REPLACE TRIGGER users_auto_increment_pk
BEFORE INSERT ON users
FOR EACH ROW
BEGIN
	:NEW.id := users_seq.NEXTVAL;
END;
/
CREATE SEQUENCE images_seq
START WITH 1 
INCREMENT BY 1 
NOMAXVALUE
NOCACHE;

CREATE OR REPLACE TRIGGER images_auto_increment_pk
BEFORE INSERT ON images
FOR EACH ROW
BEGIN
	:NEW.id := images_seq.NEXTVAL;
END;
/
CREATE SEQUENCE countries_seq
START WITH 1 
INCREMENT BY 1 
NOMAXVALUE; 

CREATE OR REPLACE TRIGGER countries_auto_increment_pk
BEFORE INSERT ON countries
FOR EACH ROW
BEGIN
	:NEW.id := countries_seq.NEXTVAL;
END;
/
CREATE SEQUENCE counties_seq
START WITH 1 
INCREMENT BY 1 
NOMAXVALUE; 

CREATE OR REPLACE TRIGGER counties_auto_increment_pk
BEFORE INSERT ON counties
FOR EACH ROW
BEGIN
	:NEW.id := counties_seq.NEXTVAL;
END;
/
CREATE SEQUENCE cities_seq
START WITH 1 
INCREMENT BY 1 
NOMAXVALUE; 

CREATE OR REPLACE TRIGGER cities_auto_increment_pk
BEFORE INSERT ON cities
FOR EACH ROW
BEGIN
	:NEW.id := cities_seq.NEXTVAL;
END;
/
CREATE SEQUENCE categories_seq
START WITH 1 
INCREMENT BY 1 
NOMAXVALUE; 

CREATE OR REPLACE TRIGGER categories_auto_increment_pk
BEFORE INSERT ON categories
FOR EACH ROW
BEGIN
	:NEW.id := categories_seq.NEXTVAL;
END;
/
-- felt�lt�s adatokkal
CREATE OR REPLACE TRIGGER user_rates
BEFORE INSERT ON ratings
FOR EACH ROW
DECLARE
  user_rate number;
  CURSOR user_rates IS 
    SELECT rate FROM ratings 
    WHERE user_id = :NEW.user_id AND image_id = :NEW.image_id;
BEGIN
  OPEN user_rates;
  FETCH user_rates INTO user_rate;
	IF(user_rates%FOUND)THEN
    DELETE FROM ratings WHERE user_id = :NEW.user_id AND image_id = :NEW.image_id;
  END IF;
END;
/

INSERT INTO COUNTRIES (NAME) VALUES('Magyarorsz�g');
INSERT INTO COUNTRIES (NAME) VALUES('N�metorsz�g');

INSERT INTO COUNTIES (COUNTRY_ID, NAME) VALUES (1, 'B�k�s megye');
INSERT INTO COUNTIES (COUNTRY_ID, NAME) VALUES (1, 'Csongr�d megye');
INSERT INTO COUNTIES (COUNTRY_ID, NAME) VALUES (1, 'Pest megye');
INSERT INTO COUNTIES (COUNTRY_ID, NAME) VALUES (2, 'N�met megye1');
INSERT INTO COUNTIES (COUNTRY_ID, NAME) VALUES (2, 'N�met megye2');
INSERT INTO COUNTIES (COUNTRY_ID, NAME) VALUES (2, 'N�met megye3');

INSERT INTO CITIES (COUNTY_ID, NAME) VALUES (1, 'Orosh�za');
INSERT INTO CITIES (COUNTY_ID, NAME) VALUES (1, 'B�k�scsaba');
INSERT INTO CITIES (COUNTY_ID, NAME) VALUES (1, 'Gyula');
INSERT INTO CITIES (COUNTY_ID, NAME) VALUES (2, 'Szeged');
INSERT INTO CITIES (COUNTY_ID, NAME) VALUES (2, 'H�dmez�v�s�rhely');
INSERT INTO CITIES (COUNTY_ID, NAME) VALUES (2, 'Csongr�d');
INSERT INTO CITIES (COUNTY_ID, NAME) VALUES (3, 'Budapest');
INSERT INTO CITIES (COUNTY_ID, NAME) VALUES (3, 'Cegl�d');
INSERT INTO CITIES (COUNTY_ID, NAME) VALUES (3, 'Hatvan');
INSERT INTO CITIES (COUNTY_ID, NAME) VALUES (4, 'Auswitczh');
INSERT INTO CITIES (COUNTY_ID, NAME) VALUES (4, 'Berlin');
INSERT INTO CITIES (COUNTY_ID, NAME) VALUES (4, 'Leningrad');
INSERT INTO CITIES (COUNTY_ID, NAME) VALUES (5, 'Bukarest');
INSERT INTO CITIES (COUNTY_ID, NAME) VALUES (5, 'Ich Will');
INSERT INTO CITIES (COUNTY_ID, NAME) VALUES (5, 'Ramsteinn');
INSERT INTO CITIES (COUNTY_ID, NAME) VALUES (6, 'Arnold');
INSERT INTO CITIES (COUNTY_ID, NAME) VALUES (6, 'Swajcenegger');
INSERT INTO CITIES (COUNTY_ID, NAME) VALUES (6, 'Algy�');

INSERT INTO categories (NAME) VALUES('Aut�');
INSERT INTO categories (NAME) VALUES('Vonat');
INSERT INTO categories (NAME) VALUES('T�jk�p');
INSERT INTO categories (NAME) VALUES('Vicces');
INSERT INTO categories (NAME) VALUES('Informatika');
INSERT INTO categories (NAME) VALUES('N�k');
INSERT INTO categories (NAME) VALUES('Receptek');