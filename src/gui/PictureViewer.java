package gui;

import adatb.beans.Image;
import adatb.common.ComboHelper;
import adatb.connect.DBConnection;
import com.sun.istack.internal.NotNull;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tomi on 2016. 04. 28..
 */
public class PictureViewer extends JFrame {

    private JPanel contentPane;
    private WebEngine webEngine;
    private List<Image> url;
    private List<Image> filteredListOfImages;
    private int currentImage;
    private List<String> categories;
    private List<String> categoriesToShow;
    private List<String> cities;
    private List<String> citiesToShow;
    private JComboBox comboPicCategory;
    private JComboBox comboPicCity;
    private JButton picPrev;
    private JButton picNext;
    private JLabel labelPicCity;
    private JLabel labelPicAvgRate;
    private JSpinner picRating;

    /**
     * Create the frame.
     */
    public PictureViewer(String title, @NotNull final List<Image> images) {
        this.url = images;
        filteredListOfImages = new ArrayList<>();
        categories = new ArrayList<>();
        cities = new ArrayList<>();

        for (Image image : url) {
            if (!categories.contains(image.getCategory())) {
                categories.add(image.getCategory());
            }
            if (!cities.contains(image.getCityName()) && image.getCategory().equals(categories.get(0))) {
                cities.add(image.getCityName());
            }
            if (image.getCategory().equals(categories.get(0)) && image.getCityName().equals(cities.get(0))) {
                filteredListOfImages.add(image);
            }
        }
        categoriesToShow = new ArrayList<>();
        for (String s : categories) {
            categoriesToShow.add(s + " (" + getNumberOfImagesByCategory(s) + ")");
        }
        citiesToShow = new ArrayList<>();
        for (String s : cities) {
            citiesToShow.add(s + " (" + getNumberOfImagesByCategoryAndCity(categories.get(0), s) + ")");
        }

        currentImage = 0;
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setBounds(100, 100, 1024, 768);
        setTitle(title);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);


        picPrev = new JButton("Előző kép");
        picPrev.setBounds(245, 660, 90, 23);
        picPrev.setEnabled(false);
        contentPane.add(picPrev);

        picNext = new JButton("Köv. kép");
        picNext.setBounds(645, 660, 89, 23);
        if (currentImage == filteredListOfImages.size() - 1) {
            picNext.setEnabled(false);
        }
        contentPane.add(picNext);

        JButton picRateButton = new JButton("Értékelem!");
        picRateButton.setBounds(435, 660, 100, 23);
        contentPane.add(picRateButton);

        int ratingOfImageByUser = 0;
        try {
            ratingOfImageByUser = DBConnection.getInstance().getRatingOfImageByUser(filteredListOfImages.get(currentImage).getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        SpinnerModel model = new SpinnerNumberModel(ratingOfImageByUser, 1, 5, 1);
        picRating = new JSpinner(model);
        picRating.setBounds(463, 626, 53, 23);
        contentPane.add(picRating);

		/*Ebbe a panelbe kéne megjeleníteni a képeket*/

        final JFXPanel picPanel = new JFXPanel();
        picPanel.setBounds(10, 11, 988, 605);
        contentPane.add(picPanel);

		/*Arra gondoltam, hogy a kategória mellett a mennyisége is megjelenhetne a comboboxban a szöveg mellett zárójelben */

        comboPicCategory = new JComboBox(categoriesToShow.toArray());
        comboPicCategory.setBounds(72, 661, 130, 20);
        contentPane.add(comboPicCategory);

        comboPicCity = new JComboBox(citiesToShow.toArray());
        comboPicCity.setBounds(72, 692, 130, 20);
        contentPane.add(comboPicCity);

        JLabel labelTypeCity = new JLabel("Kategória/Város");
        labelTypeCity.setBounds(72, 630, 130, 20);
        contentPane.add(labelTypeCity);

		/* Itt ugye a kép városát fogjuk kiirni */

        labelPicCity = new JLabel("Kép városa*");
        labelPicCity.setBounds(800, 630, 200, 20);
        contentPane.add(labelPicCity);

		/* Ide meg ugye átlagos értékelés */

        labelPicAvgRate = new JLabel("#ÉRTÉK");
        labelPicAvgRate.setBounds(800, 664, 200, 14);
        contentPane.add(labelPicAvgRate);
        setVisible(true);

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                initFX(picPanel);
            }

            private void initFX(JFXPanel fxPanel) {
                // This method is invoked on the JavaFX thread
                Scene scene = createScene();
                fxPanel.setScene(scene);
            }

            private Scene createScene() {
                StackPane root = new StackPane();
                Scene scene = new Scene(root);
                WebView webView = new WebView();
                webEngine = webView.getEngine();
                showImage(currentImage);
                root.getChildren().add(webView);

                return scene;
            }
        });

        //region listeners
        picNext.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (currentImage < filteredListOfImages.size() - 1) {
                    showImage(++currentImage);
                }
            }
        });
        picPrev.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (currentImage > 0) {
                    showImage(--currentImage);
                }
            }
        });
        picRateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    DBConnection.getInstance().uploadRateToImage(filteredListOfImages.get(currentImage).getId(), (Integer) picRating.getValue());
                    labelPicAvgRate.setText("Értékelés: " + String.format("%.1f", DBConnection.getInstance().getUsersRatingOfImage(filteredListOfImages.get(currentImage).getId())));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        comboPicCategory.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                refreshCities();
            }
        });
        comboPicCity.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                refreshFilter();
            }
        });
        //endregion
    }

    private void showImage(final int id) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                webEngine.load(filteredListOfImages.get(id).getUrl());
                if (id == 0) {
                    picPrev.setEnabled(false);
                    if (!picNext.isEnabled() && id < filteredListOfImages.size() - 1) {
                        picNext.setEnabled(true);
                    }
                }
                if (id == filteredListOfImages.size() - 1) {
                    picNext.setEnabled(false);
                    if (!picPrev.isEnabled() && id > 0) {
                        picPrev.setEnabled(true);
                    }
                }
                labelPicCity.setText("Kép városa: " + filteredListOfImages.get(currentImage).getCityName());
                try {
                    labelPicAvgRate.setText("Értékelés: " + String.format("%.1f", DBConnection.getInstance().getUsersRatingOfImage(filteredListOfImages.get(currentImage).getId())));
                    picRating.setValue(DBConnection.getInstance().getRatingOfImageByUser(filteredListOfImages.get(currentImage).getId()));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void refreshFilter() {
        filteredListOfImages = new ArrayList<>();
        int selectedCategory = comboPicCategory.getSelectedIndex();
        int selectedCity = comboPicCity.getSelectedIndex();
        if (selectedCity < 0 || selectedCity >= cities.size()) {
            selectedCity = 0;
        }
        for (Image image : url) {
            if (image.getCategory().equals(categories.get(selectedCategory)) && image.getCityName().equals(cities.get(selectedCity))) {
                filteredListOfImages.add(image);
            }
        }
        currentImage = 0;
        showImage(currentImage);
    }

    private void refreshCities() {
        cities = new ArrayList<>();
        int selectedCategory = comboPicCategory.getSelectedIndex();
        for (Image image : url) {
            if (image.getCategory().equals(categories.get(selectedCategory)) && !cities.contains(image.getCityName())) {
                cities.add(image.getCityName());
            }
        }
        citiesToShow = new ArrayList<>();
        for (String s : cities) {
            citiesToShow.add(s + " (" + getNumberOfImagesByCategoryAndCity(categories.get(selectedCategory), s) + ")");
        }
        ComboHelper.refreshComboB(comboPicCity, citiesToShow);
    }

    private int getNumberOfImagesByCategory(String category) {
        int result = 0;
        for (Image image : url) {
            if (image.getCategory().equals(category)) {
                result++;
            }
        }
        return result;
    }

    private int getNumberOfImagesByCategoryAndCity(String category, String city) {
        int result = 0;
        for (Image image : url) {
            if (image.getCategory().equals(category) && image.getCityName().equals(city)) {
                result++;
            }
        }
        return result;
    }
}