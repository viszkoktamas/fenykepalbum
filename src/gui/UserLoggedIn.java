package gui;

import adatb.beans.User;
import adatb.common.Common;
import adatb.connect.DBConnection;
import javafx.util.Pair;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

/**
 * Created by Tomi on 2016. 04. 28..
 */
public class UserLoggedIn extends JFrame {

    private JPanel contentPane;
    private JButton buttonMyPictures;

    /**
     * Create the frame.
     */
    public UserLoggedIn() {
        setTitle("Fénykép album");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 490, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel laberUser = new JLabel("Név:");
        laberUser.setBounds(10, 11, 46, 14);
        contentPane.add(laberUser);

        JLabel labelCity = new JLabel("Város:");
        labelCity.setBounds(10, 36, 46, 14);
        contentPane.add(labelCity);

        JButton buttonUpload = new JButton("Képfeltöltés");
        buttonUpload.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        try {
                            new PicUpload(UserLoggedIn.this);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        buttonUpload.setBounds(10, 61, 120, 23);
        contentPane.add(buttonUpload);

        buttonMyPictures = new JButton("Képeim");
        try {
            if (DBConnection.getInstance().getImages(Common.LOGGED_IN_USER.getId(), null, null).size() == 0) {
                buttonMyPictures.setEnabled(false);
            }
        } catch (SQLException e) {
            buttonMyPictures.setEnabled(false);
            e.printStackTrace();
        }
        buttonMyPictures.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        try {
                            if (Common.PICTURE_VIEWER != null) {
                                Common.PICTURE_VIEWER.dispose();
                            }
                            Common.PICTURE_VIEWER = new PictureViewer("Képeim", DBConnection.getInstance().getImages(Common.LOGGED_IN_USER.getId(), null, null));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        buttonMyPictures.setBounds(137, 227, 89, 23);
        contentPane.add(buttonMyPictures);

        JButton buttonGallery = new JButton("Galéria");
        buttonGallery.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        try {
                            if (Common.PICTURE_VIEWER != null) {
                                Common.PICTURE_VIEWER.dispose();
                            }
                            Common.PICTURE_VIEWER = new PictureViewer("Képeim", DBConnection.getInstance().getImages(null, null, null));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        buttonGallery.setBounds(236, 227, 89, 23);
        contentPane.add(buttonGallery);

        JButton buttonStat = new JButton("Statisztika");
        buttonStat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
            }
        });
        buttonStat.setBounds(335, 227, 120, 23);
        contentPane.add(buttonStat);

        JLabel labelGetName = new JLabel(Common.LOGGED_IN_USER.getUsername());
        labelGetName.setBounds(66, 11, 132, 14);
        contentPane.add(labelGetName);

        JLabel labelGetCity = new JLabel(Common.LOGGED_IN_USER.getCityName());
        labelGetCity.setBounds(66, 36, 132, 14);
        contentPane.add(labelGetCity);

        JButton buttonCitiesFaces = new JButton("Városok arcai");
        buttonCitiesFaces.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Common.CITY_FACES = new CityFaces("Városok arcai", DBConnection.getInstance().getImages(null, null, null));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        buttonCitiesFaces.setBounds(10, 227, 117, 23);
        contentPane.add(buttonCitiesFaces);

        JButton buttonCategories = new JButton("Kategóriák legjobb képei");
        buttonCategories.setBounds(10, 161, 200, 23);
        buttonCategories.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Common.CITY_FACES = new CityFaces("Kategóriák", DBConnection.getInstance().getImages(null, null, null));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        contentPane.add(buttonCategories);

        JButton buttonMostImage = new JButton("Legtöbb kép");
        buttonMostImage.setBounds(10, 194, 117, 23);
        buttonMostImage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    java.util.List<Pair<User, Integer>> users = DBConnection.getInstance().getUsersWhoseUploadedTheMostImages();
                    String result = "";
                    for(int i = 0; i < users.size(); i++){
                        result += "\n"+(i+1)+".: "+users.get(i).getKey().getUsername()+" : "+users.get(i).getValue();
                    }
                    JOptionPane.showMessageDialog(UserLoggedIn.this, result);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        contentPane.add(buttonMostImage);

        JButton buttonLogOut = new JButton("Log out");
        buttonLogOut.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Common.LOGGED_IN_USER = null;
                Common.MAIN_WINDOW = new MainWindow();
                dispose();
            }
        });
        buttonLogOut.setBounds(140, 61, 89, 23);
        contentPane.add(buttonLogOut);

        setVisible(true);
    }

    public void imageUploaded() {
        if (!buttonMyPictures.isEnabled()) {
            buttonMyPictures.setEnabled(true);
        }
    }
}
