package gui;

import adatb.common.AddressHelper;
import adatb.connect.DBConnection;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;

import static adatb.common.ComboHelper.refreshComboB;

/**
 * Created by Tomi on 2016. 04. 28..
 */
public class PicUpload extends JFrame {

    private JPanel contentPane;
    private JTextField urlTextField;
    private AddressHelper addressHelper;
    private JComboBox countries;
    private JComboBox counties;
    private JComboBox cities;
    private JComboBox categories;
    private List<String> categoryList;

    /**
     * Create the frame.
     */
    public PicUpload(UserLoggedIn userLoggedIn) {
        DBConnection dbc = DBConnection.getInstance();
        try {
            addressHelper = new AddressHelper(dbc.getCountriesListFromDb(), dbc.getCountiesListFromDb(), dbc.getCitiesListFromDb());
            categoryList = dbc.getCategoriesListFromDb();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        init(userLoggedIn);
    }

    private void init(final UserLoggedIn userLoggedIn) {
        setTitle("Képfeltöltés");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setBounds(100, 100, 450, 200);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel URL = new JLabel("URL:");
        URL.setBounds(10, 24, 32, 22);
        contentPane.add(URL);

        urlTextField = new JTextField();
        urlTextField.setBounds(52, 25, 372, 20);
        contentPane.add(urlTextField);
        urlTextField.setColumns(10);

        JButton btnUpload = new JButton("Feltöltés");
        btnUpload.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (urlTextField.getText().contains("http://kepfeltoltes.hu/view/")) {
                    String url = urlTextField.getText().replace("view/", "");
                    int countryId = countries.getSelectedIndex() + 1;
                    int countyId = addressHelper.getCountyId(countryId, counties.getSelectedIndex() + 1);
                    int cityId = addressHelper.getCityId(countryId, countyId, cities.getSelectedIndex() + 1);
                    try {
                        DBConnection.getInstance().uploadPicture(
                                url,
                                categories.getSelectedIndex() + 1,
                                cityId
                        );
                        userLoggedIn.imageUploaded();
                        JOptionPane.showMessageDialog(PicUpload.this, "Sikeres képfeltöltés!");
                        PicUpload.this.dispose();
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                        JOptionPane.showMessageDialog(PicUpload.this, "Képfeltöltési hiba!");
                    }
                }
            }
        });
        btnUpload.setBounds(335, 56, 89, 23);
        contentPane.add(btnUpload);

        countries = new JComboBox();
        countries.setBounds(52, 56, 150, 23);
        refreshComboBox(1);
        contentPane.add(countries);

        counties = new JComboBox();
        counties.setBounds(52, 90, 150, 23);
        refreshComboBox(2);
        contentPane.add(counties);

        cities = new JComboBox();
        cities.setBounds(52, 124, 150, 23);
        refreshComboBox(3);
        contentPane.add(cities);

        categories = new JComboBox(categoryList.toArray());
        categories.setBounds(220, 124, 150, 23);
        contentPane.add(categories);

        countries.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                refreshComboBox(2);
                refreshComboBox(3);
            }
        });
        counties.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                refreshComboBox(3);
            }
        });

        setVisible(true);
    }

    private void refreshComboBox(int which) {
        if (addressHelper != null) {
            switch (which) {
                case 1:
                    refreshComboB(countries, addressHelper.getCountryNames());
                    break;
                case 2:
                    refreshComboB(counties, addressHelper.getCountyNames(countries.getSelectedIndex() + 1));
                    break;
                case 3:
                    refreshComboB(cities, addressHelper.getCityNames(
                            addressHelper.getCountyId(countries.getSelectedIndex() + 1, counties.getSelectedIndex() + 1)
                    ));
                    break;
            }
        }
    }
}
