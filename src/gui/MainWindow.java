package gui;

import adatb.beans.User;
import adatb.common.Common;
import adatb.connect.DBConnection;
import adatb.connect.security.SHA;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class MainWindow {

	private JFrame frame;
	private JTextField textUserName;
	private JPasswordField textPassword;

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		textUserName = new JTextField();
		textUserName.setBounds(158, 71, 116, 22);
		frame.getContentPane().add(textUserName);
		textUserName.setColumns(10);

		JLabel lblNev = new JLabel("Név:");
		lblNev.setBounds(62, 74, 56, 16);
		frame.getContentPane().add(lblNev);

		JLabel lblJelszo = new JLabel("Jelszó:");
		lblJelszo.setBounds(62, 127, 56, 16);
		frame.getContentPane().add(lblJelszo);

		JButton btnLogin = new JButton("Belépés");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String username = textUserName.getText();
				String password = textPassword.getText();
				DBConnection connection = DBConnection.getInstance();
				try {
					User user = connection.getUserFromDb(username, SHA.sha1(password));
					if (user != null) {
						Common.LOGGED_IN_USER = user;
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									new UserLoggedIn();
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});

						frame.dispose();
                        Common.MAIN_WINDOW = null;
					} else {
						JOptionPane.showMessageDialog(frame, "Hibás adatok!");
					}
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(frame,
							"Error during SQL connection\n" + e.getMessage());
				}
			}
		});
		btnLogin.setBounds(177, 183, 97, 25);
		frame.getContentPane().add(btnLogin);

		textPassword = new JPasswordField();
		textPassword.setBounds(158, 127, 116, 22);
		frame.getContentPane().add(textPassword);

		JButton btnRegister = new JButton("Regisztráció");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new RegisterWindow();
			}
		});
		btnRegister.setBounds(177, 216, 107, 25);
		frame.getContentPane().add(btnRegister);

		frame.setVisible(true);
	}
}
