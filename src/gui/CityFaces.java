package gui;

import adatb.beans.Image;
import adatb.connect.DBConnection;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.util.Pair;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Tomi on 2016. 05. 01..
 */
public class CityFaces extends JFrame {

    private JPanel contentPane;
    private Map<String, List<String>> filteredList;
    private List<Image> imageList;
    private List<String> cities;
    private List<String> categories;
    private WebView webView1;
    private WebView webView2;
    private WebView webView3;
    private WebEngine webEngine1;
    private WebEngine webEngine2;
    private WebEngine webEngine3;

    public CityFaces(String title, List<Image> images) {
        filteredList = new HashMap<>();
        imageList = images;
        if (title.toLowerCase().contains("városok")) {
            cities = new ArrayList<>();
        } else {
            categories = new ArrayList<>();
        }
        for (Image i : imageList) {
            if (cities != null && !cities.contains(i.getCityName())) {
                cities.add(i.getCityName());
            } else if (categories != null && !categories.contains(i.getCategory())) {
                categories.add(i.getCategory());
            }
        }
        setTitle(title);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setBounds(100, 100, 1024, 768);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        final JFXPanel cityPicPan1 = new JFXPanel();
        cityPicPan1.setBounds(10, 11, 489, 350);
        contentPane.add(cityPicPan1);

        final JFXPanel cityPicPan2 = new JFXPanel();
        cityPicPan2.setBounds(509, 11, 489, 350);
        contentPane.add(cityPicPan2);

        final JFXPanel cityPicPan3 = new JFXPanel();
        cityPicPan3.setBounds(10, 372, 489, 350);
        contentPane.add(cityPicPan3);

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                webView1 = new WebView();
                webEngine1 = webView1.getEngine();
                webView2 = new WebView();
                webEngine2 = webView2.getEngine();
                webView3 = new WebView();
                webEngine3 = webView3.getEngine();
                List<String> list = null;
                if (cities != null) {
                    list = getImagesByCityName(cities.get(0));
                } else {
                    list = getImagesByCategoryName(categories.get(0));
                }
                initFX(cityPicPan1, webView1, webEngine1, list.get(0));
                initFX(cityPicPan2, webView2, webEngine2, list.size() > 1 ? list.get(1) : null);
                initFX(cityPicPan3, webView3, webEngine3, list.size() > 2 ? list.get(2) : null);
            }

            private void initFX(JFXPanel fxPanel, WebView webView, WebEngine webEngine, String url) {
                // This method is invoked on the JavaFX thread
                Scene scene = createScene(webView, webEngine, url);
                fxPanel.setScene(scene);
            }

            private Scene createScene(WebView webView, WebEngine webEngine, String url) {
                StackPane root = new StackPane();
                Scene scene = new Scene(root);
                if (url != null) {
                    webEngine.load(url);
                }
                root.getChildren().add(webView);
                return scene;
            }
        });

        final JComboBox cityCombo;
        if (cities != null) {
            cityCombo = new JComboBox(cities.toArray());
        } else {
            cityCombo = new JComboBox(categories.toArray());
        }
        cityCombo.setBounds(672, 517, 140, 20);
        cityCombo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (cities != null) {
                    showImages(getImagesByCityName(cities.get(cityCombo.getSelectedIndex())));
                } else {
                    showImages(getImagesByCategoryName(categories.get(cityCombo.getSelectedIndex())));
                }
            }
        });
        contentPane.add(cityCombo);
        setVisible(true);
    }

    private List<String> getImagesByCategoryName(String s) {

        if (filteredList.containsKey(s)) {
            return filteredList.get(s);
        } else {
            List<Image> list = new ArrayList<>();
            List<String> result = new ArrayList<>();
            for (Image i : imageList) {
                if (i.getCategory().equals(s)) {
                    list.add(i);
                }
            }
            return getStrings(s, list, result);
        }
    }

    private List<String> getImagesByCityName(String cityName) {
        if (filteredList.containsKey(cityName)) {
            return filteredList.get(cityName);
        } else {
            List<Image> list = new ArrayList<>();
            List<String> result = new ArrayList<>();
            for (Image i : imageList) {
                if (i.getCityName().equals(cityName)) {
                    list.add(i);
                }
            }
            return getStrings(cityName, list, result);
        }
    }

    private List<String> getStrings(String s, List<Image> list, List<String> result) {
        if (list.size() > 1) {
            for (Image i : list) {
                try {
                    i.setRating(DBConnection.getInstance().getUsersRatingOfImage(i.getId()));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        switch (list.size()) {
            case 1:
                result.add(0, list.get(0).getUrl());
                break;
            case 2:
                if (list.get(0).getRating() > list.get(1).getRating()) {
                    result.add(0, list.get(0).getUrl());
                    result.add(1, list.get(1).getUrl());
                } else {
                    result.add(0, list.get(1).getUrl());
                    result.add(1, list.get(0).getUrl());
                }
                break;
        }
        if (list.size() >= 3) {
            Pair<Image, Float> maxRate1 = new Pair<>(null, -1.f);
            Pair<Image, Float> maxRate2 = new Pair<>(null, -1.f);
            Pair<Image, Float> maxRate3 = new Pair<>(null, -1.f);
            for (Image i : list) {
                float rating = i.getRating();
                if (rating > maxRate1.getValue()) {
                    maxRate3 = maxRate2;
                    maxRate2 = maxRate1;
                    maxRate1 = new Pair<>(i, rating);
                } else if (rating > maxRate2.getValue()) {
                    maxRate3 = maxRate2;
                    maxRate2 = new Pair<>(i, rating);
                } else if (rating > maxRate3.getValue()) {
                    maxRate3 = new Pair<>(i, rating);
                }
            }
            result.add(maxRate1.getKey().getUrl());
            result.add(maxRate2.getKey().getUrl());
            result.add(maxRate3.getKey().getUrl());
        }
        filteredList.put(s, result);
        return result;
    }

    private void showImages(List<String> urls) {
        for (int i = 0; i < 3; i++) {
            switch (i) {
                case 0:
                    showImage(webEngine1, urls.size() > i ? urls.get(i) : null);
                    break;
                case 1:
                    showImage(webEngine2, urls.size() > i ? urls.get(i) : null);
                    break;
                case 2:
                    showImage(webEngine3, urls.size() > i ? urls.get(i) : null);
                    break;
            }
        }
    }

    private void showImage(final WebEngine webEngine, final String url) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                webEngine.load(url);
            }
        });
    }
}