package gui;

import adatb.beans.User;
import adatb.common.AddressHelper;
import adatb.connect.DBConnection;
import adatb.exceptions.UserAlreadyExistsException;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import static adatb.common.ComboHelper.refreshComboB;

public class RegisterWindow {

    private JFrame frame;
    private JTextField txtUserName;
    private JPasswordField txtPassword1;
    private JPasswordField txtPassword2;
    private JComboBox countries;
    private JComboBox counties;
    private JComboBox cities;

    private AddressHelper addressHelper;

    /**
     * Create the application.
     */
    public RegisterWindow() {

        DBConnection dbc = DBConnection.getInstance();
        try {
            addressHelper = new AddressHelper(dbc.getCountriesListFromDb(), dbc.getCountiesListFromDb(), dbc.getCitiesListFromDb());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 350, 400);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        //region username
        JLabel lblFelhNv = new JLabel("Felh. név:");
        lblFelhNv.setBounds(31, 40, 74, 16);
        frame.getContentPane().add(lblFelhNv);

        txtUserName = new JTextField();
        txtUserName.setBounds(132, 40, 116, 22);
        frame.getContentPane().add(txtUserName);
        txtUserName.setColumns(10);
        //endregion
        //region password
        JLabel lblJelsz = new JLabel("Jelszó:");
        lblJelsz.setBounds(31, 80, 56, 16);
        frame.getContentPane().add(lblJelsz);

        JLabel lblJelszjra = new JLabel("Jelszó újra:");
        lblJelszjra.setBounds(31, 110, 74, 16);
        frame.getContentPane().add(lblJelszjra);

        txtPassword1 = new JPasswordField();
        txtPassword1.setBounds(132, 80, 116, 22);
        frame.getContentPane().add(txtPassword1);

        txtPassword2 = new JPasswordField();
        txtPassword2.setBounds(132, 110, 116, 22);
        frame.getContentPane().add(txtPassword2);
        //endregion
        //region country
        {
            JLabel lblCountry = new JLabel("Ország:");
            lblCountry.setBounds(31, 150, 74, 16);
            frame.getContentPane().add(lblCountry);
            countries = new JComboBox();
            countries.setBounds(132, 150, 150, 22);
            refreshComboB(countries, addressHelper.getCountryNames());
            frame.getContentPane().add(countries);
        }
        //endregion
        //region county
        {
            JLabel lblCounty = new JLabel("Megye:");
            lblCounty.setBounds(31, 180, 74, 16);
            frame.getContentPane().add(lblCounty);
            counties = new JComboBox();
            counties.setBounds(132, 180, 150, 22);

            refreshComboB(counties, addressHelper.getCountyNames(countries.getSelectedIndex() + 1));
            frame.getContentPane().add(counties);
        }
        //endregion
        //region city
        {
            JLabel lblCity = new JLabel("Város:");
            lblCity.setBounds(31, 210, 74, 16);
            frame.getContentPane().add(lblCity);
            cities = new JComboBox();
            cities.setBounds(132, 210, 150, 22);
            refreshComboB(cities, addressHelper.getCityNames(
                    addressHelper.getCountyId(countries.getSelectedIndex() + 1, counties.getSelectedIndex() + 1)
            ));
            frame.getContentPane().add(cities);
        }
        //endregion

        countries.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                refreshComboB(counties, addressHelper.getCountyNames(countries.getSelectedIndex() + 1));
                refreshComboB(cities, addressHelper.getCityNames(
                        addressHelper.getCountyId(countries.getSelectedIndex() + 1, counties.getSelectedIndex() + 1)
                ));
            }
        });
        counties.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                refreshComboB(cities, addressHelper.getCityNames(
                        addressHelper.getCountyId(countries.getSelectedIndex() + 1, counties.getSelectedIndex() + 1)
                ));
            }
        });

        JButton btnRegister = new JButton("Regisztráció");
        btnRegister.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                register();
            }
        });
        btnRegister.setBounds(132, 260, 116, 25);
        frame.getContentPane().add(btnRegister);
        frame.setVisible(true);
    }

    private void register() {
        String userName = txtUserName.getText();
        String pass1 = txtPassword1.getText();
        String pass2 = txtPassword2.getText();
        int countryId = countries.getSelectedIndex() + 1;
        int countyId = addressHelper.getCountyId(countryId, counties.getSelectedIndex() + 1);
        int cityId = addressHelper.getCityId(countryId, countyId, cities.getSelectedIndex() + 1);
        String city = addressHelper.getCityNameById(cityId);

        if (userName.isEmpty() || pass1.isEmpty()) {
            JOptionPane.showMessageDialog(frame,
                    "Minden mező megadása kötelező!");
        } else if (!pass1.equals(pass2)) {
            JOptionPane.showMessageDialog(frame,
                    "A két jelszó nem egyezik meg!");
        } else if (pass1.length() < 6) {
            JOptionPane.showMessageDialog(frame,
                    "A jelszó legalább 6 karakteres legyen!");
        } else {
            DBConnection connection = DBConnection.getInstance();
            User user = new User(userName, pass1, city, cityId);
            try {
                connection.registerUser(user);
                JOptionPane.showMessageDialog(frame, "Sikeres regisztráció!");
                frame.dispose();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(frame,
                        "Hiba az SQL utasítás során: " + e.getMessage());
            } catch (UserAlreadyExistsException e) {
                JOptionPane.showMessageDialog(frame,
                        "A felhasználónév már foglalt!");
            }
        }
    }
}
