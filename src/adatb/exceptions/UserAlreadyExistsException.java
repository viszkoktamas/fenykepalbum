package adatb.exceptions;

public class UserAlreadyExistsException extends Exception {
	public UserAlreadyExistsException() {
		super();
	}

	public UserAlreadyExistsException(Throwable throwable) {
		super(throwable);
	}

	public UserAlreadyExistsException(String message) {
		super(message);
	}

	public UserAlreadyExistsException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
