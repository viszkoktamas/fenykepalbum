package adatb.beans;

import adatb.connect.security.SHA;

public class User {
    private int id;
    private String username;
    private String password;
    private String cityName;
    private int cityId;

    public User(int id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = SHA.sha1(password);
    }

    public User(String username, String password, String cityName, int cityId) {
        this.username = username;
        this.password = SHA.sha1(password);
        this.cityName = cityName;
        this.cityId = cityId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }
}
