package adatb.beans;

public class County extends AddressElement{

    private int countryId;

    public County(int id, String name, int countryId) {
        super(id, name);
        this.countryId = countryId;
    }

    public int getCountryId() {
        return countryId;
    }
}
