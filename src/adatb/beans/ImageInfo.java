package adatb.beans;

/**
 * Created by Tomi on 2016. 05. 01..
 *
 */
public class ImageInfo {

    private int imageId;
    private int userId;

    public ImageInfo(int imageId, int userId) {
        this.imageId = imageId;
        this.userId = userId;
    }

    public int getImageId() {
        return imageId;
    }

    public int getUserId() {
        return userId;
    }
}
