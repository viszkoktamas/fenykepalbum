package adatb.beans;

/**
 * Created by Tomi on 2016. 04. 28..
 *
 */
public abstract class AddressElement {

    private int id;
    private String name;

    public AddressElement(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
