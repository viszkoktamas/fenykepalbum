package adatb.beans;

/**
 * Created by Tomi on 2016. 04. 28..
 *
 */
public class City extends AddressElement {

    private int countyId;

    public City(int id, String name, int countyId) {
        super(id, name);
        this.countyId = countyId;
    }

    public int getCountyId() {
        return countyId;
    }
}
