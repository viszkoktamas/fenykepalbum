package adatb.common;

import adatb.beans.User;
import gui.CityFaces;
import gui.MainWindow;
import gui.PictureViewer;

public class Common {
    public static User LOGGED_IN_USER = null;
    public static MainWindow MAIN_WINDOW = null;
    public static PictureViewer PICTURE_VIEWER = null;
    public static CityFaces CITY_FACES = null;
}
