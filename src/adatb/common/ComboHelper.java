package adatb.common;

import javax.swing.*;

/**
 * Created by Tomi on 2016. 04. 28..
 */
public class ComboHelper {

    public static void refreshComboB(JComboBox box, java.util.List<String> list) {
        if (list != null && list.size() > 0) {
            box.removeAllItems();
            for (String s : list) {
                box.addItem(s);
            }
        }
    }
}
