package adatb.common;

import adatb.beans.AddressElement;
import adatb.beans.City;
import adatb.beans.Country;
import adatb.beans.County;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tomi on 2016. 04. 28..
 */
public class AddressHelper {

    private List<Country> countries;
    private List<County> counties;
    private List<City> cities;

    public AddressHelper(List<Country> countries, List<County> counties, List<City> cities) {
        this.countries = countries;
        this.counties = counties;
        this.cities = cities;
    }

    private List<String> getNamesFromList(List<? extends AddressElement> list) {
        List<String> result = new ArrayList<>();
        for (AddressElement e : list) {
            result.add(e.getName());
        }
        return result;
    }

    public List<String> getCountryNames() {
        return getNamesFromList(countries);
    }

    public List<String> getCountyNames(int countryId) {
        List<County> tmpList = new ArrayList<>();
        for (County c : counties) {
            if (c.getCountryId() == countryId) {
                tmpList.add(c);
            }
        }
        return getNamesFromList(tmpList);
    }

    public List<String> getCityNames(int countyId) {
        List<City> tmpList = new ArrayList<>();
        for (City c : cities) {
            if (c.getCountyId() == countyId) {
                tmpList.add(c);
            }
        }
        return getNamesFromList(tmpList);
    }

    public int getCountyId(int countryId, int localCountyId) {
        int tmp = 1;
        for (Country c : countries) {
            if (c.getId() == countryId) {
                for (County county : counties) {
                    if (county.getCountryId() == countryId) {
                        if (tmp == localCountyId) {
                            return county.getId();
                        } else {
                            tmp++;
                        }
                    }
                }
            }
        }
        return 0;
    }

    public int getCityId(int countryId, int countyId, int localCityId) {
        int tmpCity = 1;
        for (Country c : countries) {
            if (c.getId() == countryId) {
                for (County county : counties) {
                    if (county.getId() == countyId) {
                        for (City city : cities) {
                            if (city.getCountyId() == countyId) {
                                if (tmpCity == localCityId) {
                                    return city.getId();
                                } else {
                                    tmpCity++;
                                }
                            }
                        }
                    }
                }
            }
        }
        return 0;
    }

    public String getCountryNameById(int id) {
        return countries.get(id - 1).getName();
    }

    public String getCountyNameById(int id) {
        return counties.get(id - 1).getName();
    }

    public String getCityNameById(int id) {
        return cities.get(id - 1).getName();
    }
}
