package adatb.connect;

import adatb.beans.*;
import adatb.common.Common;
import adatb.exceptions.UserAlreadyExistsException;
import com.sun.istack.internal.NotNull;
import javafx.util.Pair;
import oracle.jdbc.driver.OracleDriver;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBConnection {
    private static final String DB_HOST = "localhost";
    private static final String DB_PORT = "1521";
    private static final String DB_SID = "xe";
    private static final String DB_USER = "fenykepalbum";
    private static final String DB_SEPARATOR_CHAR = ":";
    private static final String DB_PASSWORD = "Almafa1a";

    Connection c;
    private boolean isLive;

    private static final DBConnection INSTANCE = new DBConnection();

    private DBConnection() {
        isLive = false;
    }

    public static DBConnection getInstance() {
        return INSTANCE;
    }

    private void connect() throws SQLException {
        DriverManager.registerDriver(new OracleDriver());
        String url = "jdbc:oracle:thin:@" + DB_HOST + DB_SEPARATOR_CHAR
                + DB_PORT + DB_SEPARATOR_CHAR + DB_SID;
        c = DriverManager.getConnection(url, DB_USER, DB_PASSWORD);
        isLive = true;
    }

    private void disconnect() throws SQLException {
        if (isLive) {
            c.close();
            isLive = false;
        }
    }

    public User getUserFromDb(String username, @NotNull String password) throws SQLException {
        User user = null;
        try {
            if (!isLive) {
                connect();
            }

            String stmt = "SELECT * FROM users WHERE username = ? AND password = ?";
            PreparedStatement preparedStatement = c.prepareStatement(stmt);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                user = new User(rs.getInt(1), username, password);
            }
            if (user != null) {
                stmt = "SELECT city_id FROM userplaces WHERE user_id = ?";
                preparedStatement = c.prepareStatement(stmt);
                preparedStatement.setInt(1, user.getId());
                rs = preparedStatement.executeQuery();

                if (rs != null && rs.next()) {
                    user.setCityId(rs.getInt(1));
                }

                stmt = "SELECT name FROM cities WHERE id = ?";
                preparedStatement = c.prepareStatement(stmt);
                preparedStatement.setInt(1, user.getCityId());
                rs = preparedStatement.executeQuery();

                if (rs != null && rs.next()) {
                    user.setCityName(rs.getString(1));
                }
            }

        } finally {
            disconnect();
        }

        return user;
    }

    public void registerUser(User user) throws SQLException, UserAlreadyExistsException {
        User userFromDb = getUserFromDb(user.getUsername(), user.getPassword());

        if (userFromDb != null) {
            throw new UserAlreadyExistsException("User already exists!");
        }

        try {
            if (!isLive) {
                connect();
            }
            String stmt = "INSERT INTO users (username, password) VALUES(?, ?)";
            PreparedStatement preparedStatement = c.prepareStatement(stmt, new String[]{"ID"});
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.executeUpdate();

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    user.setId(generatedKeys.getInt(1));
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }

            stmt = "INSERT INTO userplaces (user_id, city_id) VALUES(?, ?)";
            preparedStatement = c.prepareStatement(stmt);
            preparedStatement.setInt(1, user.getId());
            preparedStatement.setInt(2, user.getCityId());
            preparedStatement.executeUpdate();
        } finally {
            disconnect();
        }
    }

    public List<City> getCitiesListFromDb() throws SQLException {
        List<City> result = new ArrayList<>();
        try {
            if (!isLive) {
                connect();
            }

            String stmt = "SELECT * FROM cities";
            PreparedStatement preparedStatement = c.prepareStatement(stmt);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                result.add(new City(rs.getInt(1), rs.getString(2), rs.getInt(3)));
            }
        } finally {
            disconnect();
        }
        return result;

    }

    public List<String> getCategoriesListFromDb() throws SQLException {
        List<String> result = new ArrayList<>();
        try {
            if (!isLive) {
                connect();
            }

            String stmt = "SELECT name FROM categories";
            PreparedStatement preparedStatement = c.prepareStatement(stmt);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                result.add(rs.getString(1));
            }
        } finally {
            disconnect();
        }
        return result;
    }

    public List<County> getCountiesListFromDb() throws SQLException {
        List<County> result = new ArrayList<>();
        try {
            if (!isLive) {
                connect();
            }

            String stmt = "SELECT * FROM counties";
            PreparedStatement preparedStatement = c.prepareStatement(stmt);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                result.add(new County(rs.getInt(1), rs.getString(2), rs.getInt(3)));
            }
        } finally {
            disconnect();
        }
        return result;
    }

    public List<Country> getCountriesListFromDb() throws SQLException {
        List<Country> result = new ArrayList<>();
        try {
            if (!isLive) {
                connect();
            }

            String stmt = "SELECT * FROM countries";
            PreparedStatement preparedStatement = c.prepareStatement(stmt);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                result.add(new Country(rs.getInt(1), rs.getString(2)));
            }
        } finally {
            disconnect();
        }
        return result;
    }

    public void uploadPicture(String url, int categoryId, int cityId) throws SQLException {
        try {
            int imageID;
            if (!isLive) {
                connect();
            }
            String stmt = "INSERT INTO images (hyperlink) VALUES(?)";
            PreparedStatement preparedStatement = c.prepareStatement(stmt, new String[]{"ID"});
            preparedStatement.setString(1, url);
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();

            if (generatedKeys.next()) {
                imageID = generatedKeys.getInt(1);
            } else {
                throw new SQLException("Creating image failed, no ID obtained.");
            }

            stmt = "INSERT INTO imageinfo (image_id, user_id, city_id, category_id) VALUES(?, ?, ?, ?)";
            preparedStatement = c.prepareStatement(stmt);
            preparedStatement.setInt(1, imageID);
            preparedStatement.setInt(2, Common.LOGGED_IN_USER.getId());
            preparedStatement.setInt(3, cityId);
            preparedStatement.setInt(4, categoryId);
            preparedStatement.executeUpdate();
        } finally {
            disconnect();
        }
    }

    public List<Image> getImages() throws SQLException {
        List<Image> result = new ArrayList<>();
        try {
            if (!isLive) {
                connect();
            }
            String stmt = "SELECT * FROM images";
            PreparedStatement preparedStatement = c.prepareStatement(stmt);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                result.add(new Image(rs.getInt(1), rs.getString(2)));
            }

            stmt = "SELECT * FROM imageinfo";
            preparedStatement = c.prepareStatement(stmt);
            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                Image tmpImage = result.get(rs.getInt(1) - 1);
                tmpImage.setUserId(rs.getInt(2));
                tmpImage.setCityId(rs.getInt(3));
                tmpImage.setCategoryId(rs.getInt(4));
            }

            List<City> cities = getCitiesListFromDb();

            for (Image image : result) {
                image.setCityName(cities.get(image.getCityId() - 1).getName());
            }

            List<String> categories = getCategoriesListFromDb();

            for (Image image : result) {
                image.setCategory(categories.get(image.getCategoryId() - 1));
            }

        } finally {
            disconnect();
        }
        return result;
    }

    public List<Image> getImages(Integer userId, Integer categoryId, Integer cityId) throws SQLException {
        List<Image> result = new ArrayList<>();
        List<Image> images = getImages();
        if (userId != null && categoryId == null && cityId == null) {
            for (Image image : images) {
                if (image.getUserId() == userId) {
                    result.add(image);
                }
            }
            return result;
        }
        return images;
    }

    public float getUsersRatingOfImage(int imageId) throws SQLException {
        try {
            if (!isLive) {
                connect();
            }

            String stmt = "SELECT AVG(rate) FROM ratings WHERE image_id = ?";
            PreparedStatement preparedStatement = c.prepareStatement(stmt);
            preparedStatement.setInt(1, imageId);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                return rs.getFloat(1);
            }
        } finally {
            disconnect();
        }
        return 0;
    }

    public int getRatingOfImageByUser(int imageId) throws SQLException {
        try {
            if (!isLive) {
                connect();
            }

            String stmt = "SELECT rate FROM ratings WHERE image_id = ? AND user_id = ?";
            PreparedStatement preparedStatement = c.prepareStatement(stmt);
            preparedStatement.setInt(1, imageId);
            preparedStatement.setInt(2, Common.LOGGED_IN_USER.getId());
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                return rs.getInt(1);
            }
        } finally {
            disconnect();
        }
        return 1;
    }

    public void uploadRateToImage(int imageId, int rate) throws SQLException {
        try {
            if (!isLive) {
                connect();
            }

            String stmt = "INSERT INTO ratings (image_id, user_id, rate) VALUES(?, ?, ?)";
            PreparedStatement preparedStatement = c.prepareStatement(stmt);
            preparedStatement.setInt(1, imageId);
            preparedStatement.setInt(2, Common.LOGGED_IN_USER.getId());
            preparedStatement.setInt(3, rate);
            preparedStatement.executeUpdate();

        } finally {
            disconnect();
        }
    }

    public List<Pair<User, Integer>> getUsersWhoseUploadedTheMostImages() throws SQLException {
        List<Pair<Integer, Integer>> tmpResult = new ArrayList<>();
        List<Pair<User, Integer>> result = new ArrayList<>();
        Map<Integer, Integer> usersAndImages = new HashMap<>();
        try {
            if (!isLive) {
                connect();
            }
            String stmt = "SELECT image_id, user_id FROM imageinfo";
            PreparedStatement preparedStatement = c.prepareStatement(stmt);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int userId = rs.getInt(2);
                if (usersAndImages.containsKey(userId)) {
                    usersAndImages.put(userId, usersAndImages.get(userId) + 1);
                } else {
                    usersAndImages.put(userId, 1);
                }
            }
            for (Integer userId : usersAndImages.keySet()) {
                Integer imageIdCount = usersAndImages.get(userId);
                Pair<Integer, Integer> newPair = new Pair<>(userId, imageIdCount);
                switch (tmpResult.size()) {
                    case 0:
                        tmpResult.add(newPair);
                        break;
                    case 1:
                        if (tmpResult.get(0).getValue() < imageIdCount) {
                            tmpResult.add(1, tmpResult.get(0));
                            tmpResult.set(0, newPair);
                        } else {
                            tmpResult.add(1, newPair);
                        }
                        break;
                    case 2:
                        if (tmpResult.get(0).getValue() < imageIdCount) {
                            tmpResult.add(2, tmpResult.get(1));
                            tmpResult.set(1, tmpResult.get(0));
                            tmpResult.set(0, newPair);
                        } else if (tmpResult.get(1).getValue() < imageIdCount) {
                            tmpResult.add(2, tmpResult.get(1));
                            tmpResult.set(1, newPair);
                        }else {
                            tmpResult.add(2, newPair);
                        }
                        break;
                    case 3:
                        if (tmpResult.get(0).getValue() < imageIdCount) {
                            tmpResult.add(3, tmpResult.get(2));
                            tmpResult.set(2, tmpResult.get(1));
                            tmpResult.set(1, tmpResult.get(0));
                            tmpResult.set(0, newPair);
                        } else if (tmpResult.get(1).getValue() < imageIdCount) {
                            tmpResult.add(3, tmpResult.get(2));
                            tmpResult.set(2, tmpResult.get(1));
                            tmpResult.set(1, newPair);
                        } else if (tmpResult.get(2).getValue() < imageIdCount) {
                            tmpResult.add(3, tmpResult.get(2));
                            tmpResult.set(2, newPair);
                        }else {
                            tmpResult.add(3, newPair);
                        }
                        break;
                    case 4:
                        if (tmpResult.get(0).getValue() < imageIdCount) {
                            tmpResult.add(4, tmpResult.get(3));
                            tmpResult.set(3, tmpResult.get(2));
                            tmpResult.set(2, tmpResult.get(1));
                            tmpResult.set(1, tmpResult.get(0));
                            tmpResult.set(0, newPair);
                        } else if (tmpResult.get(1).getValue() < imageIdCount) {
                            tmpResult.add(4, tmpResult.get(3));
                            tmpResult.set(3, tmpResult.get(2));
                            tmpResult.set(2, tmpResult.get(1));
                            tmpResult.set(1, newPair);
                        } else if (tmpResult.get(2).getValue() < imageIdCount) {
                            tmpResult.add(4, tmpResult.get(3));
                            tmpResult.set(3, tmpResult.get(2));
                            tmpResult.set(2, newPair);
                        } else if (tmpResult.get(3).getValue() < imageIdCount) {
                            tmpResult.add(4, tmpResult.get(3));
                            tmpResult.set(3, newPair);
                        }else {
                            tmpResult.add(4, newPair);
                        }
                        break;
                    case 5:
                        if (tmpResult.get(0).getValue() < imageIdCount) {
                            tmpResult.set(4, tmpResult.get(3));
                            tmpResult.set(3, tmpResult.get(2));
                            tmpResult.set(2, tmpResult.get(1));
                            tmpResult.set(1, tmpResult.get(0));
                            tmpResult.set(0, newPair);
                        } else if (tmpResult.get(1).getValue() < imageIdCount) {
                            tmpResult.set(4, tmpResult.get(3));
                            tmpResult.set(3, tmpResult.get(2));
                            tmpResult.set(2, tmpResult.get(1));
                            tmpResult.set(1, newPair);
                        } else if (tmpResult.get(2).getValue() < imageIdCount) {
                            tmpResult.set(4, tmpResult.get(3));
                            tmpResult.set(3, tmpResult.get(2));
                            tmpResult.set(2, newPair);
                        } else if (tmpResult.get(3).getValue() < imageIdCount) {
                            tmpResult.set(4, tmpResult.get(3));
                            tmpResult.set(3, newPair);
                        } else if (tmpResult.get(4).getValue() < imageIdCount) {
                            tmpResult.set(4, newPair);
                        }
                        break;
                }
            }
            for (Pair<Integer, Integer> p : tmpResult) {
                stmt = "SELECT username, password FROM users WHERE id = ?";
                preparedStatement = c.prepareStatement(stmt);
                preparedStatement.setInt(1, p.getKey());
                rs = preparedStatement.executeQuery();
                if(rs.next()){
                    result.add(new Pair<>(new User(p.getKey(), rs.getString(1), rs.getString(2)), p.getValue()));
                }
            }
        } finally {
            disconnect();
        }

        return result;
    }
}
