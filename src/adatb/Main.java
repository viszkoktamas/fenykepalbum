package adatb;

import adatb.common.Common;
import gui.MainWindow;
import javafx.application.Platform;

public class Main {

	public static void main(String[] args) {
		Platform.setImplicitExit(false);
		Common.MAIN_WINDOW = new MainWindow();
	}

}
